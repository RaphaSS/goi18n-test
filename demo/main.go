package main

import (
    "fmt"
    "github.com/nicksnyder/go-i18n/i18n"
)

func main() {
    i18n.MustLoadTranslationFile("../translations/en-us.json")
    i18n.MustLoadTranslationFile("../translations/pt.json")

    T, _ := i18n.Tfunc(
        "pt",
        "nao-existe",
        "en",
    )

    Raphael := map[string]interface{}{"Person": "Raphael"}
    Joao := map[string]interface{}{"Person": "João"}

    fmt.Println(T("hello_world"))
    fmt.Println(T("hello_person", Raphael))
    fmt.Println(T("person_unread_email_count", 1, Raphael))
    fmt.Println(T("person_unread_email_count", 6, Joao))
}
